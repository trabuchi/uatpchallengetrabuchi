import test, { expect } from "@playwright/test";
import { HomePage } from "../pages/homepage";
import { SignupPage } from "../pages/signupPage";
import { faker } from "@faker-js/faker";

test.describe("Sign Up Tests", () => {
  let signupPage;

  test.beforeEach(async ({ page }) => {
    const homePage = new HomePage(page);
    await page.goto("/");
    await homePage.goToSignUpPage();
    signupPage = new SignupPage(page);
  });

  test("Successful Sign Up", async ({ page }) => {
    const successMessage = "Sign up successful.";
    page.once("dialog", async (dialog) => {
      expect(dialog.message()).toContain(successMessage);
      await dialog.dismiss();
    });

    await signupPage.signUp(faker.internet.email(), faker.internet.password());
  });

  test("Sign Up with Existing Username", async ({ page }) => {
    const errorMessage = "This user already exist.";
    page.once("dialog", async (dialog) => {
      expect(dialog.message()).toContain(errorMessage);
      await dialog.dismiss();
    });

    await signupPage.signUp("test", "test");
  });

  test("User does not fill the username and password field", async ({
    page,
  }) => {
    const errorMessage = "Please fill out Username and Password.";
    page.once("dialog", async (dialog) => {
      expect(dialog.message()).toContain(errorMessage);
      await dialog.dismiss();
    });

    await signupPage.signUp("", "");
  });
});
