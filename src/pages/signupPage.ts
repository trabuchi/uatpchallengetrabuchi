import { Page, Locator } from "@playwright/test";
import { enterText } from "../utils/utils";

export class SignupPage {

    readonly page: Page;
    readonly usernameInput: Locator;
    readonly passwordInput: Locator;
    readonly signupButton: Locator;
    readonly closeButton: Locator;

    constructor(page: Page) {
        this.page = page;
        this.usernameInput = page.getByLabel('Username:')
        this.passwordInput = page.getByTestId('sign-password');
        this.signupButton = page.getByRole('button', { name: 'Sign up' });
        this.closeButton = page.getByRole('button', { name: 'Close' });
    }

    async signUp(username: string, password: string) {
        await enterText(this.usernameInput, username);
        await enterText(this.passwordInput, password);
    }

    async closeModal() {
        await this.closeButton.click();
    }

}