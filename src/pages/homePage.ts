import { Locator, Page } from "@playwright/test";
import { clickElement, getElementText } from "../utils/utils";

export class HomePage {
  readonly page: Page;
  readonly homeButton: Locator;
  readonly contactUsButton: Locator;
  readonly aboutUsButton: Locator;
  readonly cartButton: Locator;
  readonly signupButton: Locator;
  readonly loginButton: Locator;
  readonly logoutButton: Locator;
  readonly welcomeUser: Locator;
  readonly nextButton: Locator;
  readonly previousButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.homeButton = page.getByRole("link", { name: "Home (current)" });
    this.contactUsButton = page.getByRole("link", { name: "Contact" });
    this.aboutUsButton = page.getByText("About us");
    this.cartButton = page.getByRole('link', { name: 'Cart', exact: true })
    this.signupButton = page.getByRole("link", { name: "Sign up" });
    this.loginButton = page.getByRole("link", { name: "Log in" });
    this.logoutButton = page.getByRole("link", { name: "Log out" });
    this.welcomeUser = page.getByTestId("nameofuser");
    this.nextButton = page.locator("#next2");
    this.previousButton = page.locator("#prev2");
  }

  async goToLoginPage() {
    await clickElement(this.loginButton);
  }

  async goToSignUpPage() {
    await clickElement(this.signupButton);
  }

  async goToContactUsPage() {
    await clickElement(this.contactUsButton);
  }

  async goToAboutUsPage() {
    await clickElement(this.aboutUsButton);
  }

  async goToCartPage() {
    await clickElement(this.cartButton);
  }

  async goToHomePage() {
    await clickElement(this.homeButton);
  }

  async getWelcomeUserText() {
    return await getElementText(this.welcomeUser);
  }
}
