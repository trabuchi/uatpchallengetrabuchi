import test, { expect } from "@playwright/test";
import { PaymentInfoPage } from "../pages/paymentInfoPage";
import { HomePage } from "../pages/homepage";
import { CartPage } from "../pages/cartPage";
import { clickElement } from "../utils/utils";
import { faker } from "@faker-js/faker";
import { allure } from "allure-playwright";

test.describe("Payment Info Test", () => {
    let homePage;
    let paymentInfoPage;
    let cartPage;

    test.beforeEach(async ({ page }) => {
        homePage = new HomePage(page);
        paymentInfoPage = new PaymentInfoPage(page);
        cartPage = new CartPage(page);
        
        await page.goto("/");
        await allure.step("Go to cart page and initiate placing an order", async () => {
            await homePage.goToCartPage();
            await cartPage.clickPlaceOrder();
        });
    });

    test("Place order without info", async ({ page }) => {
        await allure.step("Attempt to place an order without filling out the form", async () => {
            page.on("dialog", async (dialog) => {
                await allure.step("Verify dialog message for missing information", async () => {
                    expect(dialog.message()).toContain("Please fill out Name and Creditcard.");
                });
                await dialog.dismiss();
            });

            await paymentInfoPage.fillOrderDetails("", "", "", "", "", "");
            await clickElement(paymentInfoPage.purchaseButton);
        });
    });

    test("Place order with info", async ({ page }) => {
        await allure.step("Fill in order form with generated information", async () => {
            const firstName = faker.person.fullName();
            const country = faker.location.country();
            const city = faker.location.city();
            const creditCardNumber = faker.finance.creditCardNumber();
            const expiryMonth = "12";
            const expiryYear = "2023";

            await paymentInfoPage.fillOrderDetails(firstName, country, city, creditCardNumber, expiryMonth, expiryYear);
            await clickElement(paymentInfoPage.purchaseButton);
        });

        await allure.step("Validate the purchase was successful", async () => {
            await paymentInfoPage.validatePurchase();
        });
    });
});
