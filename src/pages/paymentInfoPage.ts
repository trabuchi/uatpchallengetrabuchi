import { Page, Locator, expect } from "@playwright/test";
import { clickElement, enterText, getElementText } from "../utils/utils";

export class PaymentInfoPage {
  readonly page: Page;
  readonly totalAmount: Locator;
  readonly cardHolderName: Locator;
  readonly cardHolderCountry: Locator;
  readonly cardHolderCity: Locator;
  readonly cardNumber: Locator;
  readonly expirationMonth: Locator;
  readonly expirationYear: Locator;
  readonly purchaseButton: Locator;
  readonly closeButton: Locator;
  readonly successMessage: Locator;

  constructor(page: Page) {
    this.page = page;
    this.totalAmount = page.getByTestId("totalm");
    this.cardHolderName = page.getByTestId("name");
    this.cardHolderCountry = page.getByTestId("country");
    this.cardHolderCity = page.getByTestId("city");
    this.cardNumber = page.getByTestId("card");
    this.expirationMonth = page.getByTestId("month");
    this.expirationYear = page.getByTestId("year");
    this.purchaseButton = page.getByRole("button", { name: "Purchase" });
    this.closeButton = page.getByRole("button", { name: "Close" });
    this.successMessage = page.getByRole("heading", { name: "Thank you for your purchase!" }); 
  }

    async getTotalAmount() {
        return await getElementText(this.totalAmount);
    }

    async fillOrderDetails(name: string, country: string, city: string, card: string, month: string, year: string) {
        await enterText(this.cardHolderName, name);
        await enterText(this.cardHolderCountry, country);
        await enterText(this.cardHolderCity, city);
        await enterText(this.cardNumber, card);
        await enterText(this.expirationMonth, month);
        await enterText(this.expirationYear, year);
        await clickElement(this.purchaseButton);
    }

    async validatePurchase() {
        expect(await getElementText(this.successMessage)).toContain("Thank you for your purchase!");
    }
}
