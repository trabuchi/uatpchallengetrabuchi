import { Page, Locator } from "@playwright/test";
import { clickElement, enterText } from "../utils/utils";

export class LoginPage {

    readonly page: Page;
    readonly usernameInput: Locator;
    readonly passwordInput: Locator;
    readonly loginButton: Locator;
    readonly closeButton: Locator;

    constructor(page: Page) {
        this.page = page;
        this.usernameInput = page.getByTestId('loginusername')
        this.passwordInput = page.getByTestId('loginpassword');
        this.loginButton = page.getByRole('button', { name: 'Log in' });
        this.closeButton = page.getByRole('button', { name: 'Close' });
    }

    async login(username: string, password: string) {
        await enterText(this.usernameInput, username);
        await enterText(this.passwordInput, password);
        await clickElement(this.loginButton);
    }

    async closeModal() {
        await this.closeButton.click();
    }
    
}