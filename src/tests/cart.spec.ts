import test, { expect } from "@playwright/test";
import { ProductPage } from "../pages/products/productPage";
import { ProductInfoPage } from "../pages/products/productInfo";
import { HomePage } from "../pages/homepage";
import { CartPage } from "../pages/cartPage";
import { allure } from "allure-playwright";

test.describe("Cart test", () => {
  let productPage;
  let productInfoPage;
  let homePage;
  let cartPage;

  test.beforeEach(async ({ page }) => {
    await page.goto("/");
    productPage = new ProductPage(page);
    productInfoPage = new ProductInfoPage(page);
    homePage = new HomePage(page);
    cartPage = new CartPage(page);
  });

  test.describe.configure({ retries: 5 });

  test("Add product to the cart", async ({ page }) => {
    let productPrice;

    await allure.step("Select product and add to cart", async () => {
        await productPage.selectProduct("Samsung galaxy s6");
        productPrice = await productInfoPage.getProductPrice();
        await productInfoPage.clickAddToCart();
    });

    await allure.step("Go to cart page and verify product and price", async () => {
        await homePage.goToCartPage();
        await expect(cartPage.totalAmount).toHaveText(productPrice);

        const allProducts = await cartPage.getProductNames();
        expect(allProducts).toHaveCount(1);
        expect(allProducts).toHaveText("Samsung galaxy s6");
    });
  });

  test("Remove product from the cart", async ({ page }) => {
    let priceLumia, priceSamsung;

    await allure.step("Add two products to the cart", async () => {
      await productPage.selectProduct("Nokia lumia 1520");
      priceLumia = await productInfoPage.getProductPrice();
      await productInfoPage.clickAddToCart();
      await homePage.goToHomePage();

      await productPage.selectProduct("Samsung galaxy s6");
      priceSamsung = await productInfoPage.getProductPrice();
      await productInfoPage.clickAddToCart();
    });

    await allure.step("Verify both products are in the cart with correct total price", async () => {
      const totalCartPrice = (parseFloat(priceLumia) + parseFloat(priceSamsung)).toString();
      await homePage.goToCartPage();
      await expect(cartPage.totalAmount).toHaveText(totalCartPrice, { timeout: 10000 });

      let allProducts = await cartPage.getProductNames();
      expect(allProducts).toHaveCount(2, { timeout: 10000 });
    });

    let totalCartPrice = (parseFloat(priceLumia) + parseFloat(priceSamsung)).toString();
    await allure.step("Remove one product and verify the cart updates correctly", async () => {
        await cartPage.clickDeleteButton("Nokia lumia 1520");
        await expect(cartPage.totalAmount).toHaveText((parseFloat(totalCartPrice) - parseFloat(priceLumia)).toString(), { timeout: 10000 });

        const allProducts = await cartPage.getProductNames();
        expect(allProducts).toHaveCount(1);
        expect(await cartPage.getProductName("Nokia lumia 1520")).not.toBeVisible();
    });
  });
});
