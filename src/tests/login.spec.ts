import test, { expect } from "@playwright/test";
import { HomePage } from "../pages/homepage";
import { LoginPage } from "../pages/loginPage";
import { clickElement } from "../utils/utils";
import { allure } from "allure-playwright";

test.use({ storageState: undefined });
test.describe("Login Tests", () => {
  let homePage;
  let loginPage;
  

  test.beforeEach(async ({ page }) => {
    
    homePage = new HomePage(page);
    loginPage = new LoginPage(page);
    await page.goto("/");
    await allure.step("Open the home page", async () => {});
  });

  test("Successful Login", async ({ page }) => {
    const username = "test";
    const password = "test";

    await allure.step(
      "Go to login page and enter valid credentials",
      async () => {
        await homePage.goToLoginPage();
        await loginPage.login(username, password);
      }
    );

    await allure.step("Verify user is logged in", async () => {
      await expect(homePage.welcomeUser).toHaveText(`Welcome ${username}`);
    });

    await allure.step("Logout", async () => {
      await clickElement(homePage.logoutButton);
    });
  });

  test("Unsuccessful Login", async ({ page }) => {
    const username = "test";
    const password = "wrongpassword";

    await allure.step(
      "Go to login page and enter invalid credentials",
      async () => {
        await homePage.goToLoginPage();
        page.once("dialog", async (dialog) => {
          expect(dialog.message()).toBe("Wrong password.");
          await dialog.accept();
        });
        await loginPage.login(username, password);
      }
    );
  });
});
