import { faker } from '@faker-js/faker';
import { test as setup } from '@playwright/test';

const authFile = '.auth/user.json';

setup('authenticate', async ({ request }) => {
  const user = faker.internet.userName();
  const password = faker.internet.password();
    await request.post('https://api.demoblaze.com/signup', {
        form: {
          'user': user,
          'password': password
        }
      });  



  await request.post('https://api.demoblaze.com/login', {
    form: {
      'user': user,
      'password': password
    }
  });
  await request.storageState({ path: authFile });
});