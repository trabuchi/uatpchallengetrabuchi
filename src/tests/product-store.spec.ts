import test, { expect } from '@playwright/test';
import { ProductPage } from '../pages/products/productPage';
import { ProductInfoPage } from '../pages/products/productInfo';

const productDetails = {
  phone: {
    productName: 'Samsung galaxy s6',
    productPrice: '360',
    productDescription: 'The Samsung Galaxy S6 is powered by 1.5GHz octa-core Samsung Exynos 7420 processor and it comes with 3GB of RAM. The phone packs 32GB of internal storage cannot be expanded.',
  },
  laptop: {
    productName: 'Sony vaio i5',
    productPrice: '790',
    productDescription: 'Sony is so confident that the VAIO S is a superior ultraportable laptop that the company proudly compares the notebook to Apple\'s 13-inch MacBook Pro. And in a lot of ways this notebook is better, thanks to a lighter weight.',
  },
  monitor: {
    productName: 'Apple monitor 24',
    productPrice: '400',
    productDescription: 'LED Cinema Display features a 27-inch glossy LED-backlit TFT active-matrix LCD display with IPS technology and an optimum resolution of 2560x1440. It has a 178 degree horizontal and vertical viewing angle, a "typical" brightness of 375 cd/m2, contrast ratio of 1000:1, and a 12 ms response time.',
  },
};

test.describe('Product Store', () => {
  test.beforeEach(async ({ page }) => {
    await page.goto('/');
  });

  test('View Phone Details', async ({ page }) => {
    const product = new ProductPage(page);
    const productInfo = new ProductInfoPage(page);

    await product.getProductCategories('Phones');

    await product.selectProduct(productDetails.phone.productName);
    const selectedProductDetails = await productInfo.getProductDetails();

    expect(selectedProductDetails).toEqual(productDetails.phone);
  });

  test('View Laptop Details', async ({ page }) => {
    const product = new ProductPage(page);
    const productInfo = new ProductInfoPage(page);

    await product.getProductCategories('Laptops');

    await product.selectProduct(productDetails.laptop.productName);
    const selectedProductDetails = await productInfo.getProductDetails();

    expect(selectedProductDetails).toEqual(productDetails.laptop);
  });

  test('View Monitor Details', async ({ page }) => {
    const product = new ProductPage(page);
    const productInfo = new ProductInfoPage(page);

    await product.getProductCategories('Monitors');

    await product.selectProduct(productDetails.monitor.productName);
    const selectedProductDetails = await productInfo.getProductDetails();

    expect(selectedProductDetails).toEqual(productDetails.monitor);
  });
});
