import { Locator, Page } from "@playwright/test";
import { clickElement, getElementText } from "../utils/utils";

export class CartPage {
  readonly page: Page;
  readonly placeOrder: Locator;
  readonly totalAmount: Locator;
  readonly productNames: Locator;
  
  

  constructor(page: Page) {
    this.page = page;
    this.placeOrder = page.getByRole("button", { name: "Place Order" });
    this.totalAmount = page.getByTestId("totalp");
    this.productNames = page.locator('//*[@id="tbodyid"]/tr/td[2]');
  }

  async clickPlaceOrder() {
    await clickElement(this.placeOrder);
  }

  async getCartProductPrice() {
    return getElementText(this.totalAmount);
  }

  async getProductName(product: string) {
    return this.page.locator(`//td[contains(text(),'${product}')]`);
  }

  async getProductPrice(product: string) {
    return this.page.locator(
      `//td[contains(text(),'${product}')]/following-sibling::td[1]]`
    );
  }

  async clickDeleteButton(product: string) {
    await clickElement(
      this.page.locator(
        `//td[contains(text(),'${product}')]/following-sibling::td[2]/a`
      )
    );
  }


  async getProductNames() {
    return this.productNames;
  }


}


