import { Locator, Page } from "@playwright/test";
import { clickElement } from "../../utils/utils";




export class ProductPage {

    private readonly page: Page;
    private readonly productCategories: Locator;
    private readonly productName: Locator;
    private readonly productPrice: Locator;
    private readonly productContainer: Locator;
    private readonly addToCartButton: Locator;

    constructor(page: Page) {
        this.page = page;
        this.productName = page.locator('//h4[@class="card-title"]');
        
        this.productContainer = page.getByTestId('productContainer');
        this.addToCartButton = page.getByRole('button', { name: 'Add to cart' });
    }
    
    async getProductCategories(category:string) {
        await clickElement(this.page.getByRole('link', { name: category }))
    }

    async getProduct(product:string) {
        return this.page.getByRole('link', { name: product })
    }
    
    async selectProduct(product:string) {
        
        let productElement = this.page.getByRole('link', { name: product });
        await clickElement(productElement);
        
    }


    async getProductName(product:string) {
        return await this.productName.textContent();
    }

    async getProductPrice(product:string) {
        return await this.productPrice.textContent();
    }

    async getProductDetails(product:string) {
        const productDetails = await this.productContainer.textContent();
        
    }

    

}

