import { Locator } from "@playwright/test";

export async function enterText(locator: Locator, text: string) {
  await locator.clear();
  await locator.fill(text);
}



export async function getElementText(locator: Locator): Promise<string> {
  await waitForElementToBeVisible(locator);
  
  return locator.innerText();
}



export async function clickElement(locator: Locator) {
  await waitForElementToBeVisible(locator);
  await locator.click();
}

export async function waitForElementToBeVisible(locator: Locator) {
  await locator.waitFor({ state: "visible" });
}
