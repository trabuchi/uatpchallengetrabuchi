import { Locator, Page, expect } from "@playwright/test";
import { clickElement, getElementText } from "../../utils/utils";
import exp from "constants";

export class ProductInfoPage {
    private readonly page: Page;

    private readonly productName: Locator;
    private readonly productPrice: Locator;
    private readonly productDescription: Locator;
    private readonly addToCartButton: Locator;

    constructor(page: Page) {
        this.page = page;
        this.productName = page.locator('h2[class="name"]')
        this.productPrice = page.locator('h3[class="price-container"]');
        this.productDescription = page.locator('[id="more-information"] p');
        this.addToCartButton = page.getByRole('link', { name: 'Add to cart' });
    }

    async getProductName() {
        return getElementText(this.productName);
    }

    async getProductPrice() : Promise<string>{
        let price = await getElementText(this.productPrice);
         return price?.split("*").shift()?.trim().split("$").pop() ?? '';
    }

    async getProductDescription() {
        let description = await getElementText(this.productDescription);
        return description?.trim().replace(/(\r\n|\n|\r)/gm, "");
    }

    async clickAddToCart() {
        await clickElement(this.addToCartButton);
        
        
    }

    async getProductDetails() {
        const productName = await this.getProductName();
        const productPrice = await this.getProductPrice();
        const productDescription = await this.getProductDescription();
        return { productName, productPrice, productDescription };
    }

}